﻿using AutoMapper;
using PaymentApi.Domain.DTOs;
using PaymentApi.Domain.Entities;
using PaymentApi.Domain.Interfaces.Repositories;
using PaymentApi.Domain.Interfaces.Services;
using PaymentApi.Infra.CrossCutting.Exceptions;

namespace PaymentApi.Service.Services
{
    public class PedidoService : BaseService<PedidoDTO, Pedido>, IPedidoService
    {
        protected readonly IPedidoRepository _pedidoRepository;
        protected readonly IMapper _mapper;

        public PedidoService(IPedidoRepository pedidoRepository, IMapper mapper)
            : base(pedidoRepository, mapper)
        {
            _pedidoRepository = pedidoRepository;
            _mapper = mapper;
        }

        public override async Task<PedidoDTO> Create(PedidoDTO pedidoDto)
        {
            if (pedidoDto == null)
                throw new Exception("invalid object");

            var entity = _mapper.Map<Pedido>(pedidoDto);
            entity.Validate();

            var created = await _baseRepository.CreateAsync(entity);
            var itensPedido = MapItensPedido(pedidoDto.Itens, created);
            var itensPedidoCreatedDto = await AddItemPedido(itensPedido);

            if (!itensPedidoCreatedDto.Any())
            {
                await _pedidoRepository.DisposeAsync();
                throw new DomainException("Erro ao criar pedido.");
            }

            var pedido = _pedidoRepository.GetByIdAsync(created.Id);
            return _mapper.Map<PedidoDTO>(pedido);
        }

        public async Task<IList<ItemPedidoDTO>> AddItemPedido(IList<ItemPedidoDTO> itensPedidoDto)
        {
            IList<ItemPedido> itensPedidoCreatedDto = new List<ItemPedido>();
            try
            {
                foreach (var item in itensPedidoDto)
                {
                    var itemPedido = _mapper.Map<ItemPedido>(item);
                    itemPedido.Validate();

                    var itemPedidoCreated = await _pedidoRepository.AddItemPedidoAsync(itemPedido);
                    itensPedidoCreatedDto.Add(itemPedidoCreated);
                }

                await _pedidoRepository.SabeChangesAsync();
            }
            catch (Exception ex)
            {
                itensPedidoCreatedDto.Clear();
                Console.WriteLine($"Exception: {ex.Message}");
            }

            return _mapper.Map<IList<ItemPedidoDTO>>(itensPedidoCreatedDto);
        }

        #region "Metodos Privados"

        private IList<ItemPedidoDTO> MapItensPedido(IList<ItemPedidoDTO> itensPedidoDto, Pedido pedido)
        {
            IList<ItemPedido> itensPedido = new List<ItemPedido>();

            foreach (var item in itensPedidoDto)
            {
                var itemPedido = _mapper.Map<ItemPedido>(item);
                itemPedido.ChangePedido(pedido);
                itensPedido.Add(itemPedido);
            }

            return _mapper.Map<IList<ItemPedidoDTO>>(itensPedido);
        }


        #endregion
    }
}
