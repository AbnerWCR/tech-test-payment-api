﻿using AutoMapper;
using PaymentApi.Domain.DTOs;
using PaymentApi.Domain.Entities;
using PaymentApi.Domain.Interfaces.Repositories;
using PaymentApi.Domain.Interfaces.Services;

namespace PaymentApi.Service.Services
{
    public class BaseService<TDto, TEntity> : IBaseService<TDto, TEntity>
        where TDto : BaseDTO
        where TEntity : BaseEntity
    {
        protected readonly IBaseRepository<TEntity> _baseRepository;
        protected readonly IMapper _mapper;

        public BaseService(IBaseRepository<TEntity> baseRepository, IMapper mapper)
        {
            _baseRepository = baseRepository;
            _mapper = mapper;
        }

        public virtual async Task<TDto> Create(TDto obj)
        {
            if (obj == null)
                throw new Exception("invalid object");

            var entity = _mapper.Map<TEntity>(obj);
            entity.Validate();

            var created = await _baseRepository.CreateAsync(entity);

            return _mapper.Map<TDto>(created);
        }

        public virtual async Task<TDto> Update(TDto obj)
        {
            if (obj == null)
                throw new Exception("invalid object");

            var entity = _mapper.Map<TEntity>(obj);
            entity.Validate();

            var updated = await _baseRepository.UpdateAsync(entity);

            return _mapper.Map<TDto>(updated);
        }

        public virtual async Task Delete(Guid id)
        {
            await _baseRepository.DeleteAsync(id);
        }

        public virtual async Task<IList<TDto>> Get()
        {
            var entities = await _baseRepository.GetAllAsync();

            return _mapper.Map<IList<TDto>>(entities);
        }

        public virtual async Task<TDto> GetById(Guid id)
        {            
            var entity = await _baseRepository.GetByIdAsync(id);

            return _mapper.Map<TDto>(entity);
        }

    }
}
