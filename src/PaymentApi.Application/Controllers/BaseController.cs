﻿using Microsoft.AspNetCore.Mvc;
using PaymentApi.Application.ViewModels;

namespace PaymentApi.Application.Controllers
{
    public class BaseController : Controller
    {

        protected IActionResult ResultOk(string message, dynamic data)
        {
            var resultVm = new ResultViewModel(message, true, data);
            var jsonObj = resultVm.ToJson();
            return Ok(jsonObj);
        }

        protected IActionResult ResultCreated(Uri uri, string message, dynamic data)
        {
            var resultVm = new ResultViewModel(message, true, data);
            return Created(uri, resultVm.ToJson());
        }

        protected IActionResult ResultNotFound(string message)
        {
            var resultVm = new ResultViewModel(message, false, null);

            return NotFound(resultVm.ToJson());
        }

        protected IActionResult ResultInternalError(string message)
        {
            var resultVm = new ResultViewModel(message, false, null);

            return StatusCode(500, resultVm.ToJson());
        }

        protected IActionResult ResultNotAuthorized(string message)
        {
            var resultVm = new ResultViewModel(message, false, null);

            return StatusCode(401, resultVm.ToJson());
        }

        protected IActionResult ResultBadRequest(string message, dynamic data) 
        {
            var result = new ResultViewModel(message, false, data);
            return BadRequest(result);
        }
    }
}
