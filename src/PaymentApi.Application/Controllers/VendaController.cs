﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PaymentApi.Application.ViewModels;
using PaymentApi.Domain.DTOs;
using PaymentApi.Domain.Entities;
using PaymentApi.Domain.Enums;
using PaymentApi.Domain.Interfaces.Services;
using PaymentApi.Infra.CrossCutting.Exceptions;

namespace PaymentApi.Application.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class VendaController : BaseController
    {
        private readonly IPedidoService _servicePedido;
        private readonly IBaseService<VendedorDTO, Vendedor> _serviceVendedor;
        private readonly IMapper _mapper;

        public VendaController(
            IPedidoService servicePedido, 
            IBaseService<VendedorDTO, Vendedor> serviceVendedor, 
            IMapper mapper)
        {
            _servicePedido = servicePedido;
            _serviceVendedor = serviceVendedor;
            _mapper = mapper;   
        }

        #region Metodos Publicos

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> CreateVenda(
            [FromBody] CriarPedidoViewModel criarPedidoVm)
        {
            try
            {
                var vendedorVm = await CriarVendedor(criarPedidoVm.Vendedor);
                var pedidoVm = await CriarPedido(criarPedidoVm, vendedorVm.Id);
                
                var uri = new Uri($"api/v1/venda/{pedidoVm.Id}", UriKind.RelativeOrAbsolute);
                return ResultCreated(uri, "Venda criada com sucesso", pedidoVm);
            }
            catch (DomainException dmEx)
            {
                return ResultBadRequest("Verifique as informações.", dmEx);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex.Message}");
                return ResultInternalError("Erro no sistema. Contate o administrador.");
            }
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetVenda(
            Guid id)
        {
            try
            {
                var pedidoDto = await _servicePedido.GetById(id);

                if (pedidoDto == null)
                    return ResultNotFound("Pedido não encontrado.");

                var pedidoVm = _mapper.Map<PedidoViewModel>(pedidoDto);

                return ResultOk("Venda encontrada.", pedidoVm);
            }
            catch (DomainException dmEx)
            {
                return ResultBadRequest("Verifique as informações.", dmEx);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex.Message}");
                return ResultInternalError("Erro no sistema. Contate o administrador.");
            }
        }

        #endregion

        #region Metodos Privados

        private async Task<VendedorViewModel> CriarVendedor(CriarVendedorViewModel vendedorVm)
        {
            var vendedor = _mapper.Map<VendedorDTO>(vendedorVm);

            var vendedores = await _serviceVendedor.Get();

            if (vendedores.Any(x => x.Cpf == vendedor.Cpf))
                return _mapper.Map<VendedorViewModel>(vendedores.First(x => x.Cpf == vendedor.Cpf));

            var novoVendedor = await _serviceVendedor.Create(vendedor);

            return _mapper.Map<VendedorViewModel>(novoVendedor);
        }

        private async Task<PedidoViewModel> CriarPedido(CriarPedidoViewModel pedidoVm, Guid vendedorId)
        {
            var pedidoDto = _mapper.Map<PedidoDTO>(pedidoVm);
            pedidoDto.VendedorId = vendedorId;
            pedidoDto.Status = StatusPedido.AguardandoPagamento;
            pedidoDto.Data = DateTime.Now;
            var novoPedido = await _servicePedido.Create(pedidoDto);

            return _mapper.Map<PedidoViewModel>(novoPedido);
        }

        #endregion
    }
}
