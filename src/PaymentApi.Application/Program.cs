using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using PaymentApi.Application.Mapper;
using PaymentApi.Domain.Interfaces.Repositories;
using PaymentApi.Domain.Interfaces.Services;
using PaymentApi.Infra.Data.Context;
using PaymentApi.Infra.Data.Repositories;
using PaymentApi.Service.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<PaymentApiDbContext>(options =>
{
    options.UseSqlite(
        connectionString: builder.Configuration.GetConnectionString("default"))
    .EnableSensitiveDataLogging()
    .UseLoggerFactory(LoggerFactory.Create(build => build.AddConsole()));
});

#region "Auto Mapper"

var autoMapper = new MapperConfiguration(cfg =>
{
    cfg.AddMapping();
});

#region "Inje��o de depend�ncia"

builder.Services.AddSingleton(autoMapper.CreateMapper());
builder.Services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
builder.Services.AddScoped(typeof(IBaseService<,>), typeof(BaseService<,>));
builder.Services.AddScoped<IPedidoService, PedidoService>();
builder.Services.AddScoped<IPedidoRepository, PedidoRepository>();

#endregion

#endregion

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    #region  "description api"

    c.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "User API",
        Version = "v1",
        Description = "API feita para desafio da plataforma DIO.me",
        Contact = new OpenApiContact
        {
            Name = "Abner Wallace",
            Email = "abnerwcrodrigues@gmail.com"
        },
    });

    #endregion
});

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
