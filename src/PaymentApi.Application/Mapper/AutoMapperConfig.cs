﻿using AutoMapper;
using PaymentApi.Application.ViewModels;
using PaymentApi.Domain.DTOs;
using PaymentApi.Domain.Entities;

namespace PaymentApi.Application.Mapper
{
    public static class AutoMapperConfig
    {
        public static IMapperConfigurationExpression AddMapping(
            this IMapperConfigurationExpression cfg)
        {
            #region "Entity to DTO"

            cfg.CreateMap<Vendedor, VendedorDTO>()
                .ForMember(x => x.PrimeiroNome, x => x.MapFrom(u => u.Nome.PrimeiroNome))
                .ForMember(x => x.Sobrenome, x => x.MapFrom(u => u.Nome.Sobrenome))
                .ForMember(x => x.Cpf, x => x.MapFrom(u => u.Cpf.Documento))
                .ForMember(x => x.Email, x => x.MapFrom(u => u.Email.Address))
                .ForMember(x => x.Telefone, x => x.MapFrom(u => u.Telefone))
                .ReverseMap();

            cfg.CreateMap<ItemPedido, ItemPedidoDTO>()
                .ReverseMap();

            cfg.CreateMap<Pedido, PedidoDTO>()
                .ReverseMap();

            #endregion

            #region "DTO to View Model"

            #region "Vendedor"

            cfg.CreateMap<VendedorDTO, CriarVendedorViewModel>()
               .ReverseMap();

            cfg.CreateMap<VendedorDTO, VendedorViewModel>()
                .ReverseMap();

            #endregion

            #region "Itens pedido"

            cfg.CreateMap<ItemPedidoDTO, CriarItemPedidoViewModel>()
               .ReverseMap();

            cfg.CreateMap<ItemPedidoDTO, ItemPedidoViewModel>()
               .ReverseMap();

            #endregion

            #region "Pedido"

            cfg.CreateMap<PedidoDTO, CriarPedidoViewModel>()
                .ReverseMap();

            cfg.CreateMap<PedidoDTO, PedidoViewModel>()
                .ReverseMap();

            #endregion

            #endregion

            return cfg;
        }
    }
}
