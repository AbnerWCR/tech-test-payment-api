﻿using Newtonsoft.Json;
using PaymentApi.Domain.DTOs;
using PaymentApi.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace PaymentApi.Application.ViewModels
{
    public class CriarItemPedidoViewModel
    {
        [JsonProperty(PropertyName = "codigo")]
        [Required(ErrorMessage = "Codigo é obrigatório.")]
        [MaxLength(6, ErrorMessage = "Codigo deve ter no máximo 11 caracteres.")]
        [MinLength(6, ErrorMessage = "Codigo deve ter no mínimo 11 caracteres.")]
        public string Codigo { get; set; }

        [JsonProperty(PropertyName = "tipo")]
        [Required(ErrorMessage = "Tipo é obrigatório.")]
        public TipoItemVenda Tipo { get; set; }

        [JsonProperty(PropertyName = "quantidade")]
        [Required(ErrorMessage = "Quantidade é obrigatório.")]
        public double Quantidade { get; set; }

        [JsonProperty(PropertyName = "valor")]
        [Required(ErrorMessage = "Valor é obrigatório.")]
        public decimal Valor { get; set; }
    }
}
