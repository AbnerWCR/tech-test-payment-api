﻿using Newtonsoft.Json;

namespace PaymentApi.Application.ViewModels
{
    [Serializable]
    public class VendedorViewModel
    {
        [JsonProperty(PropertyName = "vendedorId")]
        public Guid Id { get; set; }

        [JsonProperty(PropertyName = "primeiroNome")]
        public string PrimeiroNome { get; set; }

        [JsonProperty(PropertyName = "sobrenome")]
        public string Sobrenome { get; set; }

        [JsonProperty(PropertyName = "cpf")]
        public string Cpf { get; set; }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "telefone")]
        public string Telefone { get; set; }
    }
}
