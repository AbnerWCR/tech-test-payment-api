﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace PaymentApi.Application.ViewModels
{
    [Serializable]
    public class CriarPedidoViewModel
    {
        [JsonProperty(PropertyName = "vendedor")]
        [Required(ErrorMessage = "Vendedor é obrigatório.")]
        public CriarVendedorViewModel Vendedor { get; set; }

        [JsonProperty(PropertyName = "itens")]
        [Required(ErrorMessage = "Itens pedido é obrigatório")]
        [MinLength(1, ErrorMessage = "O pedido deve ter, pelo menos, um item.")]
        public IList<CriarItemPedidoViewModel> Itens { get; set; }
    }
}
