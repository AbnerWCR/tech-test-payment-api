﻿using Newtonsoft.Json;
using PaymentApi.Domain.Enums;

namespace PaymentApi.Application.ViewModels
{
    [Serializable]
    public class ItemPedidoViewModel
    {
        [JsonProperty(PropertyName = "itemPedidoId")]
        public Guid Id { get; set; }

        [JsonProperty(PropertyName = "codigo")]
        public string Codigo { get; set; }

        [JsonProperty(PropertyName = "tipo")]
        public TipoItemVenda Tipo { get; set; }

        [JsonProperty(PropertyName = "quantidade")]
        public double Quantidade { get; set; }

        [JsonProperty(PropertyName = "valor")]
        public decimal Valor { get; set; }

        [JsonProperty(PropertyName = "pedidoId")]
        public Guid PedidoId { get; set; }
    }
}
