﻿using Newtonsoft.Json;
using PaymentApi.Domain.Enums;

namespace PaymentApi.Application.ViewModels
{
    [Serializable]
    public class PedidoViewModel
    {
        [JsonProperty(PropertyName = "pedidoId")]
        public Guid Id { get; set; }

        [JsonProperty(PropertyName = "data")]
        public DateTime Data { get; set; }

        [JsonProperty(PropertyName = "status")]
        public StatusPedido Status { get; set; }

        [JsonProperty(PropertyName = "vendedorId")]
        public Guid VendedorId { get; set; }

        [JsonProperty(PropertyName = "vendedor")]
        public VendedorViewModel Vendedor { get; set; }

        [JsonProperty(PropertyName = "itensPedido")]
        public IList<ItemPedidoViewModel> Itens { get; set; }
    }
}
