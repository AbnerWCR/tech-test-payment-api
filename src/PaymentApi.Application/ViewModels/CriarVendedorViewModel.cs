﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace PaymentApi.Application.ViewModels
{
    [Serializable]
    public class CriarVendedorViewModel
    {
        [JsonProperty(PropertyName = "primeiroNome")]
        [Required(ErrorMessage = "Primeiro nome é obrigatório.")]
        [MaxLength(50, ErrorMessage = "Primeiro nome deve ter no máximo 50 caracteres.")]
        [MinLength(3, ErrorMessage = "Primeiro nome deve ter no mínimo 3 caracteres.")]
        public string PrimeiroNome { get; set; }

        [JsonProperty(PropertyName = "sobrenome")]
        [Required(ErrorMessage = "Sobrenome nome é obrigatório.")]
        [MaxLength(50, ErrorMessage = "Sobrenome deve ter no máximo 50 caracteres.")]
        [MinLength(3, ErrorMessage = "Sobrenome deve ter no mínimo 3 caracteres.")]
        public string Sobrenome { get; set; }

        [JsonProperty(PropertyName = "cpf")]
        [Required(ErrorMessage = "Cpf nome é obrigatório.")]
        [MaxLength(11, ErrorMessage = "Cpf deve ter no máximo 11 caracteres.")]
        [MinLength(11, ErrorMessage = "Cpf deve ter no mínimo 11 caracteres.")]
        public string Cpf { get; set; }

        [JsonProperty(PropertyName = "email")]
        [Required(ErrorMessage = "Email nome é obrigatório.")]
        [MaxLength(180, ErrorMessage = "Email deve ter no máximo 180 caracteres.")]
        [MinLength(10, ErrorMessage = "Email deve ter no mínimo 10 caracteres.")]
        [RegularExpression(
                            @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", 
                            ErrorMessage = "Email inválido.")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "telefone")]
        [Required(ErrorMessage = "Telefone nome é obrigatório.")]
        [MaxLength(16, ErrorMessage = "Telefone deve ter no máximo 16 caracteres.")]
        [MinLength(16, ErrorMessage = "Telefone deve ter no mínimo 16 caracteres.")]
        [RegularExpression(
                            @"^([+]?[\s0-9]+)?(\d{3}|[(]?[0-9]+[)])?([-]?[\s]?[0-9])+$",
                            ErrorMessage = "Telefone inválido. Informe o número sem espaços, no formato => +55(00)000000000")]
        public string Telefone { get; set; }
    }
}
