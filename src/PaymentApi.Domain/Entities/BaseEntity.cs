﻿namespace PaymentApi.Domain.Entities
{
    public abstract class BaseEntity
    {
        public virtual Guid Id { get; set; }

        internal List<string> errors;

        public IReadOnlyCollection<string> Errors => errors;

        public abstract void Validate();
    }
}
