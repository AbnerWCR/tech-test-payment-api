﻿using PaymentApi.Domain.Validations;
using PaymentApi.Domain.VOs;
using PaymentApi.Infra.CrossCutting.Exceptions;

namespace PaymentApi.Domain.Entities
{
    public class Vendedor : BaseEntity
    {
        public NomeValueObject Nome { get; private set; }
        public CpfValueObject Cpf { get; private set; }
        public EmailValueObject Email { get; private set; }
        public TelefoneValueObject Telefone { get; private set; }
        public IList<Pedido> Pedidos { get; private set; }

        protected Vendedor()
        {

        }

        public Vendedor(NomeValueObject nome, CpfValueObject cpf, EmailValueObject email, TelefoneValueObject telefone)
        {
            Nome = nome;
            Cpf = cpf;
            Email = email;
            Telefone = telefone;
            Validate();
        }

        public Vendedor(NomeValueObject nome, CpfValueObject cpf, EmailValueObject email, TelefoneValueObject telefone, IList<Pedido> pedidos)
        {
            Nome = nome;
            Cpf = cpf;
            Email = email;
            Telefone = telefone;
            Pedidos = pedidos;
            Validate();
        }

        public override void Validate()
        {
            var validator = new VendedorValidator();
            var validation = validator.Validate(this);

            if (!validation.IsValid)
            {
                errors = validation.Errors.Select(x => x.ErrorMessage)
                                            .ToList();

                throw new DomainException("Vendedor inválido.", Errors.ToList());
            }
        }
    }
}
