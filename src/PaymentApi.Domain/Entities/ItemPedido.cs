﻿using PaymentApi.Domain.Enums;
using PaymentApi.Domain.Validations;
using PaymentApi.Infra.CrossCutting.Exceptions;

namespace PaymentApi.Domain.Entities
{
    public class ItemPedido : BaseEntity
    {
        public string Codigo { get; private set; }
        public TipoItemVenda Tipo { get; private set; }
        public double Quantidade { get; private set; }
        public decimal Valor { get; private set; }
        public decimal Total => Convert.ToDecimal(Quantidade) * Valor;
        public Guid PedidoId { get; private set; }
        public Pedido Pedido { get; private set; }

        protected ItemPedido()
        {

        }

        public ItemPedido(string codigo, TipoItemVenda tipo, double quantidade, decimal valor, Pedido pedido)
        {
            Codigo = codigo;
            Tipo = tipo;
            Quantidade = quantidade;
            Valor = valor;
            Pedido = pedido;
            Validate();            
        }

        public void ChangeCodigo(string codigo)
        {
            Codigo = codigo;
            Validate();
        }

        public void ChangeTipo(TipoItemVenda tipo)
        {
            Tipo = tipo;
            Validate();
        }

        public void ChangeQuantidade(double quantidade)
        {
            Quantidade = quantidade;
            Validate();
        }

        public void ChangeValor(decimal valor)
        {
            Valor = valor;
            Validate();
        }

        public void ChangePedido(Pedido pedido)
        {
            PedidoId = pedido.Id;
            Pedido = pedido;
            Validate();
        }

        public override void Validate()
        {
            var validator = new ItemVendaValidator();
            var validation = validator.Validate(this);

            if (!validation.IsValid)
            {
                errors = validation.Errors.Select(x => x.ErrorMessage)
                                            .ToList();

                throw new DomainException("Item da venda inválido", Errors.ToList());
            }
        }
    }
}
