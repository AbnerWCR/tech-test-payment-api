﻿using PaymentApi.Domain.Enums;
using PaymentApi.Domain.Validations;
using PaymentApi.Infra.CrossCutting.Exceptions;

namespace PaymentApi.Domain.Entities
{
    public class Pedido : BaseEntity
    {
        public DateTime Data { get; private set; }
        public StatusPedido Status { get; private set; }
        public Guid VendedorId { get; private set; }
        public Vendedor Vendedor { get; private set; }
        public IList<ItemPedido> Itens { get; private set; }

        protected Pedido()
        {
        }

        public Pedido(Vendedor vendedor, StatusPedido status, IList<ItemPedido> itens)
        {
            Vendedor = vendedor;
            Status = status;
            Itens = itens;
            Validate();
        }

        public void ChangeItemPedidoPorId(ItemPedido item)
        {
            var obj = Itens.FirstOrDefault(x => x.Id == item.Id);

            if (obj != null)
            {
                obj.ChangeCodigo(item.Codigo);
                obj.ChangeTipo(item.Tipo);
                obj.ChangeQuantidade(item.Quantidade);
                obj.ChangeValor(item.Valor);
            }
        }

        public override void Validate()
        {
            var validator = new PedidoValidator();
            var validation = validator.Validate(this);  

            if (!validation.IsValid)
            {
                errors = validation.Errors.Select(x => x.ErrorMessage)
                                            .ToList();

                throw new DomainException("Pedido inválido.", Errors.ToList());
            }
        }
    }
}
