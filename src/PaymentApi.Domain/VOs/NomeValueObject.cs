﻿using PaymentApi.Domain.Validations;
using PaymentApi.Infra.CrossCutting.Exceptions;

namespace PaymentApi.Domain.VOs
{
    public class NomeValueObject : BaseValueObject
    {
        public string PrimeiroNome { get; private set; }
        public string Sobrenome { get; private set; }

        protected NomeValueObject()
        {

        }

        public NomeValueObject(string primeiroNome, string sobrenome)
        {
            PrimeiroNome = primeiroNome;
            Sobrenome = sobrenome;
            Validate();
        }

        public void ChangePrimeiroNome(string primeiroNome)
        {
            PrimeiroNome = primeiroNome;
            Validate();
        }

        public void ChangeSobrenome(string sobrenome)
        {
            Sobrenome = sobrenome;
            Validate();
        }

        public override void Validate()
        {
            var validator = new NomeValidator();
            var validation = validator.Validate(this);

            if (!validation.IsValid)
            {
                errors = validation.Errors.Select(x => x.ErrorMessage)
                                            .ToList();

                throw new DomainException("Vendedor inválido.", Errors.ToList());
            }
        }
    }
}
