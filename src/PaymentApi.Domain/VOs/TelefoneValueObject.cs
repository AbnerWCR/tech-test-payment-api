﻿using PaymentApi.Domain.Validations;
using PaymentApi.Infra.CrossCutting.Exceptions;

namespace PaymentApi.Domain.VOs
{
    public class TelefoneValueObject : BaseValueObject
    {
        public string NumeroTelefone { get; private set; }

        protected TelefoneValueObject()
        {

        }

        public TelefoneValueObject(string numeroTelefone)
        {
            NumeroTelefone = numeroTelefone;
            Validate();
        }

        public void ChangeNumeroTelefone(string numeroTelefone)
        {
            NumeroTelefone = numeroTelefone;
            Validate();
        }

        public override void Validate()
        {
            var validator = new TelefoneValidator();
            var validation = validator.Validate(this);

            if (!validation.IsValid)
            {
                errors = validation.Errors.Select(x => x.ErrorMessage)
                                            .ToList();

                throw new DomainException("Vendedor inválido.", Errors.ToList());
            }
        }
    }
}
