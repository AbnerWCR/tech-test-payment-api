﻿using PaymentApi.Domain.Validations;
using PaymentApi.Infra.CrossCutting.Exceptions;

namespace PaymentApi.Domain.VOs
{
    public class CpfValueObject : BaseValueObject
    {
        public string Documento { get; private set; }

        protected CpfValueObject() 
        {
        }

        public CpfValueObject(string documento)
        {
            Documento = documento;
            Validate();
        }

        public void ChangeDocumento(string documento)
        {
            Documento = documento;
            Validate();
        }

        public override void Validate()
        {
            var validator = new CpfValidator();
            var validation = validator.Validate(this);

            if (!validation.IsValid)
            {
                errors = validation.Errors.Select(x => x.ErrorMessage)
                                            .ToList();

                throw new DomainException("Vendedor inválido.", Errors.ToList());
            }
        }
    }
}
