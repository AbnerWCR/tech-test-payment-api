﻿using PaymentApi.Domain.Validations;
using PaymentApi.Infra.CrossCutting.Exceptions;

namespace PaymentApi.Domain.VOs
{
    public class EmailValueObject : BaseValueObject
    {
        public string Address { get; private set; }

        protected EmailValueObject()
        {

        }

        public EmailValueObject(string address)
        {
            Address = address;
            Validate();
        }

        public void ChangeAddress(string address)
        {
            Address = address;
            Validate();
        }

        public override void Validate()
        {
            var validator = new EmailValidator();
            var validation = validator.Validate(this);

            if (!validation.IsValid)
            {
                errors = validation.Errors.Select(x => x.ErrorMessage)
                                            .ToList();

                throw new DomainException("Vendedor inválido.", Errors.ToList());
            }
        }
    }
}
