﻿namespace PaymentApi.Domain.VOs
{
    public abstract class BaseValueObject
    {
        internal List<string> errors;

        public IReadOnlyCollection<string> Errors => errors;

        public abstract void Validate();
    }
}
