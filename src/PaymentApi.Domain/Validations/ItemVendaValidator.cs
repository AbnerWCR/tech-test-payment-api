﻿using FluentValidation;
using PaymentApi.Domain.Entities;

namespace PaymentApi.Domain.Validations
{
    public class ItemVendaValidator : AbstractValidator<ItemPedido>
    {
        public ItemVendaValidator()
        {
            RuleFor(x => x)
                .NotNull()
                .WithMessage("Item da venda não pode ser nulo.");

            RuleFor(x => x.Codigo)
                .NotNull()
                .WithMessage("Codigo do item da venda não pode ser nulo.")

                .NotEmpty()
                .WithMessage("Codigo do item da venda não pode ser vazio.")

                .MinimumLength(6)
                .WithMessage("Codigo do item da venda deve ter no mínimo 6 caracteres.")

                .MaximumLength(6)
                .WithMessage("Codigo do item da venda deve ter no máximo 6 caracteres.");

            RuleFor(x => x.Quantidade)
                .LessThanOrEqualTo(0)
                .WithMessage("Quantidade do item da venda deve ter no mínimo 0.");

            RuleFor(x => x.Valor)
               .LessThanOrEqualTo(0)
               .WithMessage("Valor do item da venda deve ter no mínimo 0.");
        }
    }
}
