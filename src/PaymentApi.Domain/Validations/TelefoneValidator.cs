﻿using FluentValidation;
using PaymentApi.Domain.VOs;

namespace PaymentApi.Domain.Validations
{
    public class TelefoneValidator : AbstractValidator<TelefoneValueObject>
    {
        public TelefoneValidator()
        {
            RuleFor(x => x)
                .NotNull()
                .WithMessage("Telefone não pode ser nulo");

            RuleFor(x => x.NumeroTelefone)
               .NotNull()
               .WithMessage("Numero Telefone não pode ser nulo.")

               .NotEmpty()
               .WithMessage("Numero Telefone não pode ser vazio.")

               .MinimumLength(16)
               .WithMessage("Numero Telefone deve ter no mínimo 16 dígitos.")

               .MaximumLength(16)
               .WithMessage("Numero Telefone deve ter no máximo 16 dígitos.")

               .Matches(@"^([+]?[\s0-9]+)?(\d{3}|[(]?[0-9]+[)])?([-]?[\s]?[0-9])+$")
               .WithMessage("Numero Telefone é inválido");
        }
    }
}
