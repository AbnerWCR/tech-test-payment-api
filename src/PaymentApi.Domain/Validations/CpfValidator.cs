﻿using FluentValidation;
using PaymentApi.Domain.VOs;

namespace PaymentApi.Domain.Validations
{
    public class CpfValidator : AbstractValidator<CpfValueObject>
    {
        public CpfValidator()
        {
            RuleFor(x => x)
               .NotNull()
               .WithMessage("Cpf não pode ser nulo");

            RuleFor(x => x.Documento)
               .NotNull()
               .WithMessage("Documento do vendedor não pode ser nulo.")

               .NotEmpty()
               .WithMessage("Documento do vendedor não pode ser vazio.")

               .MinimumLength(11)
               .WithMessage("Documento deve ter no mínimo 11 caracteres.")

               .MaximumLength(11)
               .WithMessage("Documento deve ter no máximo 11 caracteres.");
        }
    }
}
