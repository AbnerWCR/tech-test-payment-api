﻿using FluentValidation;
using PaymentApi.Domain.Entities;

namespace PaymentApi.Domain.Validations
{
    public class PedidoValidator : AbstractValidator<Pedido>
    {
        public PedidoValidator()
        {
            RuleFor(x => x)
                .NotNull()
                .WithMessage("O Pedido não pode ser nulo.");

            RuleFor(x => x.Itens)
                .Must(collection => collection == null || !collection.Any())
                .WithMessage("O pedido deve ter, pelo menos, um item.");
        }
    }
}
