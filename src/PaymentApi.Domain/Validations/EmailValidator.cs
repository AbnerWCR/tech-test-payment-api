﻿using FluentValidation;
using PaymentApi.Domain.VOs;

namespace PaymentApi.Domain.Validations
{
    public class EmailValidator : AbstractValidator<EmailValueObject>
    {
        public EmailValidator()
        {
            RuleFor(x => x)
                .NotNull()
                .WithMessage("Email não pode ser nulo");

            RuleFor(x => x.Address)
               .NotNull()
               .WithMessage("Address não pode ser nulo.")

               .NotEmpty()
               .WithMessage("Address não pode ser nulo.")

               .MinimumLength(10)
               .WithMessage("Address deve ter no mínimo 11 caracteres.")

               .MaximumLength(180)
               .WithMessage("Address deve ter no máximo 11 caracteres.")

               .Matches(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$")
               .WithMessage("Address não é válido.");
        }
    }
}
