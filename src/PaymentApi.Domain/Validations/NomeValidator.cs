﻿using FluentValidation;
using PaymentApi.Domain.VOs;

namespace PaymentApi.Domain.Validations
{
    public class NomeValidator : AbstractValidator<NomeValueObject>
    {
        public NomeValidator()
        {
            RuleFor(x => x.PrimeiroNome)
                .NotNull()
                .WithMessage("Primeiro Nome não pode ser nulo.")

                .NotEmpty()
                .WithMessage("Primeiro Nome não pode ser vazio.")

                .MinimumLength(10)
                .WithMessage("Primeiro Nome deve ter no mínimo 10 caracteres.")

                .MaximumLength(50)
                .WithMessage("Primeiro Nome deve ter no máximo 50 caracteres.");

            RuleFor(x => x.Sobrenome)
                .NotNull()
                .WithMessage("Sobrenome Nome não pode ser nulo.")

                .NotEmpty()
                .WithMessage("Sobrenome Nome não pode ser vazio.")

                .MinimumLength(10)
                .WithMessage("Sobrenome Nome deve ter no mínimo 10 caracteres.")

                .MaximumLength(50)
                .WithMessage("Sobrenome Nome deve ter no máximo 50 caracteres.");
        }
    }
}
