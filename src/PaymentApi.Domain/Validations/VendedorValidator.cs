﻿using FluentValidation;
using PaymentApi.Domain.Entities;

namespace PaymentApi.Domain.Validations
{
    public class VendedorValidator : AbstractValidator<Vendedor>
    {
        public VendedorValidator()
        {
            RuleFor(x => x)
                .NotNull()
                .WithMessage("Vendedor não pode ser nulo.");
        }
    }
}
