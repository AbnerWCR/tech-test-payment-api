﻿using System.Text.Json.Serialization;

namespace PaymentApi.Domain.Enums
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum TipoItemVenda
    {
        Papelaria = 1,
        Brinquedos = 2,
        Informatica = 3
    }
}
