﻿using System.Text.Json.Serialization;

namespace PaymentApi.Domain.Enums
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum StatusPedido
    {
        AguardandoPagamento = 1,
        PagamentoAprovado = 2,
        EnviadoParaTransportadora = 3,
        Entregue = 4, 
        Cancelado = 5
    }
}
