﻿using PaymentApi.Domain.Entities;

namespace PaymentApi.Domain.Interfaces.Repositories
{
    public interface IPedidoRepository : IBaseRepository<Pedido>
    {
        Task<ItemPedido> AddItemPedidoAsync(ItemPedido itemPedido);
        Task SabeChangesAsync();
        Task DisposeAsync();
    }
}
