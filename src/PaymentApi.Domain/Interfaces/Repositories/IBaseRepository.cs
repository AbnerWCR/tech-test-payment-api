﻿using PaymentApi.Domain.Entities;

namespace PaymentApi.Domain.Interfaces.Repositories
{
    public interface IBaseRepository<TEntity> 
        where TEntity : BaseEntity
    {
        Task<TEntity> CreateAsync(TEntity entity);
        Task<TEntity> UpdateAsync(TEntity entity);
        Task DeleteAsync(Guid id);
        Task<TEntity> GetByIdAsync(Guid id);
        Task<IList<TEntity>> GetAllAsync();
    }
}
