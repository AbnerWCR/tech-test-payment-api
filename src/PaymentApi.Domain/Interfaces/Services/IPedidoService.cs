﻿using PaymentApi.Domain.DTOs;
using PaymentApi.Domain.Entities;

namespace PaymentApi.Domain.Interfaces.Services
{
    public interface IPedidoService : IBaseService<PedidoDTO, Pedido>
    {
        Task<IList<ItemPedidoDTO>> AddItemPedido(IList<ItemPedidoDTO> itensPedido);
    }
}
