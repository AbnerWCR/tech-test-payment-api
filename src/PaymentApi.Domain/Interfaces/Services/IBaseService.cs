﻿using PaymentApi.Domain.DTOs;
using PaymentApi.Domain.Entities;

namespace PaymentApi.Domain.Interfaces.Services
{
    public interface IBaseService<TDto, TEntity>
        where TDto : BaseDTO
        where TEntity : BaseEntity
    {
        Task<TDto> Create(TDto obj);

        Task<TDto> Update(TDto obj);

        Task Delete(Guid id);

        Task<IList<TDto>> Get();

        Task<TDto> GetById(Guid id);
    }
}
