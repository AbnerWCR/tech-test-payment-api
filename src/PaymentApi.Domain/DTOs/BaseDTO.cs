﻿namespace PaymentApi.Domain.DTOs
{
    public abstract class BaseDTO
    {
        public virtual Guid Id { get; set; }
    }
}
