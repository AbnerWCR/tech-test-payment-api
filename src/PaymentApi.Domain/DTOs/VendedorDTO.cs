﻿namespace PaymentApi.Domain.DTOs
{
    public class VendedorDTO : BaseDTO
    {
        public string PrimeiroNome { get; set; }
        public string Sobrenome { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        public IList<PedidoDTO> Pedidos { get; set; }
    }
}
