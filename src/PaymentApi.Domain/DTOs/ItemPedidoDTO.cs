﻿using PaymentApi.Domain.Enums;

namespace PaymentApi.Domain.DTOs
{
    public class ItemPedidoDTO : BaseDTO
    {
        public string Codigo { get; set; }
        public TipoItemVenda Tipo { get; set; }
        public double Quantidade { get; set; }
        public decimal Valor { get; set; }
        public decimal Total => Convert.ToDecimal(Quantidade) * Valor;
        public Guid PedidoId { get; set;}
        public PedidoDTO Pedido { get; set; }
    }
}
