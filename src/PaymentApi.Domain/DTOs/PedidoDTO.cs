﻿using PaymentApi.Domain.Enums;

namespace PaymentApi.Domain.DTOs
{
    public class PedidoDTO : BaseDTO
    {
        public DateTime Data { get; set; }
        public StatusPedido Status { get; set; }
        public Guid VendedorId { get; set; }
        public VendedorDTO Vendedor { get; set; }
        public IList<ItemPedidoDTO> Itens { get; set; }
    }
}
