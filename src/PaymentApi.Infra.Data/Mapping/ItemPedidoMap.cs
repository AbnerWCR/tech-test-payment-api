﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PaymentApi.Domain.Entities;

namespace PaymentApi.Infra.Data.Mapping
{
    public class ItemPedidoMap : IEntityTypeConfiguration<ItemPedido>
    {
        public void Configure(EntityTypeBuilder<ItemPedido> builder)
        {
            builder.ToTable("ITENS_PEDIDO");

            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id)
                .HasColumnName("ID");

            builder.Property(x => x.Codigo)
                .IsRequired()
                .HasColumnType("varchar(6)")
                .HasColumnName("CODIGO");

            builder.Property(x => x.Tipo)
                .IsRequired()
                .HasColumnType("integer")
                .HasColumnName("TIPO");

            builder.Property(x => x.Quantidade)
                .IsRequired()
                .HasColumnType("double")
                .HasColumnName("QUANTIDADE");

            builder.Property(x => x.Valor)
                .IsRequired()
                .HasColumnType("decimal(10,2)")
                .HasColumnName("VALOR");

            builder.HasOne(x => x.Pedido)
                .WithMany(x => x.Itens)
                .HasForeignKey(x => x.PedidoId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
