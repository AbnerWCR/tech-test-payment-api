﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PaymentApi.Domain.Entities;

namespace PaymentApi.Infra.Data.Mapping
{
    public class PedidoMap : IEntityTypeConfiguration<Pedido>
    {
        public void Configure(EntityTypeBuilder<Pedido> builder)
        {
            builder.ToTable("PEDIDOS");

            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id)
                .HasColumnName("ID");

            builder.Property(x => x.Data)
                .HasColumnName("DATA")
                .HasColumnType("datetime");

            builder.Property(x => x.Status)
                .HasColumnName("STATUS")
                .HasColumnType("smallint");

            builder.HasOne(x => x.Vendedor)
                .WithMany(x => x.Pedidos)
                .HasForeignKey(x => x.VendedorId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
