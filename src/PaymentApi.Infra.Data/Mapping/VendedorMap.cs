﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PaymentApi.Domain.Entities;

namespace PaymentApi.Infra.Data.Mapping
{
    internal class VendedorMap : IEntityTypeConfiguration<Vendedor>
    {
        public void Configure(EntityTypeBuilder<Vendedor> builder)
        {
            builder.ToTable("VENDEDORES");

            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id)
                .HasColumnName("ID");

            builder.OwnsOne(x => x.Nome)
                .Property(x => x.PrimeiroNome)
                .IsRequired()
                .HasColumnType("varchar(50)")
                .HasColumnName("PRIMEIRO_NOME");

            builder.OwnsOne(x => x.Nome)
                .Property(x => x.Sobrenome)
                .HasColumnType("varchar(50)")
                .HasColumnName("SOBRENOME");

            builder.OwnsOne(x => x.Cpf)
                .Property(x => x.Documento)
                .IsRequired()
                .HasColumnType("varchar(11)")
                .HasColumnName("CPF");

            builder.OwnsOne(x => x.Email)
                .Property(x => x.Address)
                .IsRequired()
                .HasColumnType("varchar(180)")
                .HasColumnName("EMAIL");

            builder.OwnsOne(x => x.Telefone)
                .Property(x => x.NumeroTelefone)
                .IsRequired()
                .HasColumnType("varhcar(16)")
                .HasColumnName("TELEFONE");
        }
    }
}
