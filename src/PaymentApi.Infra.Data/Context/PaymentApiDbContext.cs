﻿using Microsoft.EntityFrameworkCore;
using PaymentApi.Domain.Entities;
using PaymentApi.Infra.Data.Mapping;

namespace PaymentApi.Infra.Data.Context
{
    public class PaymentApiDbContext : DbContext
    {
        public PaymentApiDbContext()
        {

        }

        public PaymentApiDbContext(DbContextOptions<PaymentApiDbContext> options)
            :base(options)
        {

        }

        public DbSet<Pedido> Pedidos { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<ItemPedido> ItensPedidos { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new VendedorMap());
            builder.ApplyConfiguration(new PedidoMap());
            builder.ApplyConfiguration(new ItemPedidoMap());

            base.OnModelCreating(builder);
        }
    }
}
