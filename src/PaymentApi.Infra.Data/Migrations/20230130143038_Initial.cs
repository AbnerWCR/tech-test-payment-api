﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PaymentApi.Infra.Data.Migrations
{
    /// <inheritdoc />
    public partial class Initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "VENDEDORES",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "TEXT", nullable: false),
                    PRIMEIRONOME = table.Column<string>(name: "PRIMEIRO_NOME", type: "varchar(50)", nullable: false),
                    SOBRENOME = table.Column<string>(type: "varchar(50)", nullable: false),
                    CPF = table.Column<string>(type: "varchar(11)", nullable: false),
                    EMAIL = table.Column<string>(type: "varchar(180)", nullable: false),
                    TELEFONE = table.Column<string>(type: "varhcar(13)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VENDEDORES", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "PEDIDOS",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "TEXT", nullable: false),
                    DATA = table.Column<DateTime>(type: "datetime", nullable: false),
                    STATUS = table.Column<int>(type: "smallint", nullable: false),
                    VendedorId = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PEDIDOS", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PEDIDOS_VENDEDORES_VendedorId",
                        column: x => x.VendedorId,
                        principalTable: "VENDEDORES",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ITENS_PEDIDO",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "TEXT", nullable: false),
                    CODIGO = table.Column<string>(type: "varchar(6)", nullable: false),
                    TIPO = table.Column<long>(type: "integer", nullable: false),
                    QUANTIDADE = table.Column<double>(type: "double", nullable: false),
                    VALOR = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    PedidoId = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ITENS_PEDIDO", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ITENS_PEDIDO_PEDIDOS_PedidoId",
                        column: x => x.PedidoId,
                        principalTable: "PEDIDOS",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ITENS_PEDIDO_PedidoId",
                table: "ITENS_PEDIDO",
                column: "PedidoId");

            migrationBuilder.CreateIndex(
                name: "IX_PEDIDOS_VendedorId",
                table: "PEDIDOS",
                column: "VendedorId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ITENS_PEDIDO");

            migrationBuilder.DropTable(
                name: "PEDIDOS");

            migrationBuilder.DropTable(
                name: "VENDEDORES");
        }
    }
}
