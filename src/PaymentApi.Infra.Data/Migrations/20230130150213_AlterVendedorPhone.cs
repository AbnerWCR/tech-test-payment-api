﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PaymentApi.Infra.Data.Migrations
{
    /// <inheritdoc />
    public partial class AlterVendedorPhone : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "TELEFONE",
                table: "VENDEDORES",
                type: "varhcar(16)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varhcar(13)");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "TELEFONE",
                table: "VENDEDORES",
                type: "varhcar(13)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varhcar(16)");
        }
    }
}
