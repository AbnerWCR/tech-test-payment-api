﻿using PaymentApi.Domain.Entities;
using PaymentApi.Domain.Interfaces.Repositories;
using PaymentApi.Infra.Data.Context;

namespace PaymentApi.Infra.Data.Repositories
{
    public class PedidoRepository : BaseRepository<Pedido>, IPedidoRepository
    {
        private readonly PaymentApiDbContext _context;

        public PedidoRepository(PaymentApiDbContext context)
            : base(context) 
        {
            _context = context;
        }

        public override async Task<Pedido> CreateAsync(Pedido pedido)
        {
            await _context.AddAsync(pedido);
            return pedido;
        }

        public async Task<ItemPedido> AddItemPedidoAsync(ItemPedido itemPedido)
        {
            await _context.AddAsync(itemPedido);
            return itemPedido;
        }

        public async Task SabeChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        public async Task DisposeAsync()
        {
            await _context.DisposeAsync();
        }
    }
}
