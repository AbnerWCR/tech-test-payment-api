﻿using Microsoft.EntityFrameworkCore;
using PaymentApi.Domain.Entities;
using PaymentApi.Domain.Interfaces.Repositories;
using PaymentApi.Infra.Data.Context;

namespace PaymentApi.Infra.Data.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity>
        where TEntity : BaseEntity
    {
        private readonly PaymentApiDbContext _context;

        public BaseRepository(PaymentApiDbContext context)
        {
            _context = context;
        }

        public virtual async Task<TEntity> CreateAsync(TEntity entity)
        {
            await _context.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public virtual async Task<TEntity> UpdateAsync(TEntity entity)
        {
            _context.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return entity;
        }

        public virtual async Task DeleteAsync(Guid id)
        {
            var obj = GetByIdAsync(id);

            if (obj != null) 
            {
                _context.Remove(obj);
                await _context.SaveChangesAsync();
            }
        }

        public virtual async Task<IList<TEntity>> GetAllAsync()
        {
            return await _context.Set<TEntity>()
                                    .AsNoTracking()
                                    .ToListAsync();
        }

        public virtual async Task<TEntity> GetByIdAsync(Guid id)
        {
            var entities =  await _context.Set<TEntity>()
                                            .AsNoTracking()
                                            .Where(x => x.Id == id)
                                            .ToListAsync();

            return entities.FirstOrDefault();
        }
    }
}
